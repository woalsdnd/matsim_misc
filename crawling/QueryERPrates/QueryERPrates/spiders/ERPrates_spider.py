import scrapy
import json
import time

import sys
sys.path.append('/usr/local/lib/python2.7/dist-packages/')
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.select import Select

from QueryERPrates.items import QueryerpratesItem

class ERPrates(scrapy.Spider):
    name = "ERPrates"
    start_urls = ["http://www.mytransport.sg/content/mytransport/home/myconcierge/erprates.html"]

    def __init__(self):
        self.driver = webdriver.Firefox()
            
    def parse(self,response):
        
        self.driver.get(response.url)

        for i in range(77):
            self.driver.get(response.url)
            
            #select erp gantry and click map button
            button=WebDriverWait(self.driver,100).until(lambda driver: self.driver.find_element_by_xpath("//input[@type='button']"))

            #index of first element is 1
            Select(self.driver.find_element_by_id("erp_gantry")).select_by_index(i+1)
            Select(self.driver.find_element_by_id("erp_vcc_day")).select_by_index(1)
            button.click()
        
            #get necessary elements (after waiting for the page loading)
            WebDriverWait(self.driver,100).until(lambda driver: self.driver.find_element_by_class_name('erp_result_ti'))
            el_times = self.driver.find_elements_by_class_name('erp_result_ti')
            el_prices = self.driver.find_elements_by_class_name('erp_result_pr')

            #extract texts
            times=[el.text for el in el_times] 
            prices=[el.text for el in el_prices] 
            
            #yield info
            item=QueryerpratesItem()
            item['time_interval']=times
            item['price']=prices
            yield item
