import scrapy
import json
import time

import sys
sys.path.append('/usr/local/lib/python2.7/dist-packages/')
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait


from PcodeGoogleMap.items import PcodegooglemapItem

class PcodeGoogleMapSpider(scrapy.Spider):
    name = "PGM"
    start_urls = ["https://www.google.com.sg/maps/"]

    def __init__(self):
        self.driver = webdriver.Firefox()
            
    def parse(self,response):
        
        #open postal code file
        with open('/home/jaemin/Research/MATSIM/demand_generation/infer_loc_by_pcode/matsim_sg_loc_ret_input_output/pcode') as f:
            lines=f.read().splitlines()
            
        #start crawling    
        self.driver.get(response.url)

        for line in lines:
                        
            #wait for page loading
            loc_field=WebDriverWait(self.driver,100).until(lambda driver: self.driver.find_element_by_id('searchboxinput'))
            button=WebDriverWait(self.driver,100).until(lambda driver: self.driver.find_element_by_css_selector('.searchbox-searchbutton-container'))

            #type in postal code
            loc_field.clear()
            loc_field.send_keys(line)
            button.click()
        
            #wait for page load (5 seconds)
            time.sleep(5)
            
            #parse retrieved url
            loc=self.driver.current_url
            for i in range(6):
                loc=loc[loc.find("/")+1:]
            loc=loc[loc.find("@")+1:loc.find("/",loc.find("@"))]
            tmp_list=loc.strip().split(',')

            item=PcodegooglemapItem()
            if len(tmp_list)>2:
                item['latitude']=tmp_list[0]
                item['longitude']=tmp_list[1]
                item['pcode']=line
            else:
                print "Location parsing failed : "+loc
                print "Parsed results"+str(tmp_list)
                
            yield item
