import scrapy
import json
import time

import sys
sys.path.append('/usr/local/lib/python2.7/dist-packages/')
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.select import Select

from QueryERP.items import QueryerpItem

class ERPspider(scrapy.Spider):
    name = "ERP"
    start_urls = ["http://www.mytransport.sg/content/mytransport/home/myconcierge/erprates.html"]

    def __init__(self):
        self.driver = webdriver.Firefox()
            
    def parse(self,response):
        
        self.driver.get(response.url)

        for i in range(77):
            
            #select erp gantry and click map button
            button=WebDriverWait(self.driver,100).until(lambda driver: self.driver.find_element_by_id('erp_map'))

            #index of first element is 1
            Select(self.driver.find_element_by_id("erp_gantry")).select_by_index(i+1)
            button.click()
        
            #wait until popup
            #time.sleep(3)
            WebDriverWait(self.driver,100).until(lambda driver: len(self.driver.window_handles)>1)

            #switch control to popup
            handle=self.driver.window_handles
            self.driver.switch_to_window(handle[1])
            
            #parse retrieved url
            loc=self.driver.current_url
            for i in range(4):
                loc=loc[loc.find("/")+1:]

            item=QueryerpItem()
            
            item['longitude']=loc[loc.find("lon=")+4:loc.find("&",loc.find("lon="))]
            item['latitude']=loc[loc.find("lat=")+4:]

            
            #close popup
            self.driver.close()
            #push control back to the starting window
            #self.driver.select_window()
            self.driver.switch_to_window(handle[0])
                
            yield item
