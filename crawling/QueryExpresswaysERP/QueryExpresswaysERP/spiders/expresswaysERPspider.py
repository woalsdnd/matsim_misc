import scrapy
import json
import time

import sys
sys.path.append('/usr/local/lib/python2.7/dist-packages/')
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select

from QueryExpresswaysERP.items import QueryexpresswayserpItem

class ERPspider(scrapy.Spider):
    name = "expresswaysERP"
    start_urls = ["http://www.mytransport.sg/content/mytransport/home/myconcierge/erprates.html"]

    def __init__(self):
        self.driver = webdriver.Firefox()
            
    def parse(self,response):
        
        self.driver.get(response.url)

        #index of first element from 9 to 15
        for i in range(9,16):
            
            #select ith element from the drop-down for erp road
            Select(self.driver.find_element_by_id("erp_road")).select_by_index(i)
            WebDriverWait(self.driver,100).until(lambda driver: self.driver.find_element_by_id('erp_road'))
                        
            #get the number of the gantries
            n_gantries=len(Select(self.driver.find_element_by_id("erp_gantry")).options)
            
            for j in range(1,n_gantries):
                Select(self.driver.find_element_by_id("erp_gantry")).select_by_index(j)
            
                #define the button
                button=WebDriverWait(self.driver,100).until(EC.visibility_of_element_located((By.ID,'erp_map')))
                button.click()
            
                #wait until popup
                WebDriverWait(self.driver,100).until(lambda driver: len(self.driver.window_handles)>1)

                #switch control to popup
                handle=self.driver.window_handles
                self.driver.switch_to_window(handle[1])
                
                #parse retrieved url
                loc=self.driver.current_url
                for i in range(4):
                    loc=loc[loc.find("/")+1:]
    
                item=QueryexpresswayserpItem()
                
                item['longitude']=loc[loc.find("lon=")+4:loc.find("&",loc.find("lon="))]
                item['latitude']=loc[loc.find("lat=")+4:]
    
                
                #close popup
                self.driver.close()
                #push control back to the starting window
                #self.driver.select_window()
                self.driver.switch_to_window(handle[0])
                    
                yield item
