import json
import ogr, osr

def coordinate_converter():
   
    #open postal_code, longitude, latitude file
    with open('/home/jaemin/Research/MATSIM/demand_generation/infer_loc_by_pcode/matsim_sg_loc_ret_input_output/pcode_lon_lat.json') as f:
        lines=json.load(f)

    #set transformation rule
    source=osr.SpatialReference()
    source.ImportFromEPSG(4326)#WGS84
    target=osr.SpatialReference()
    target.ImportFromEPSG(3414)#SVY21 (Singapore coordinate)
    
    transform=osr.CoordinateTransformation(source,target)
    
    for i in range(len(lines)):
        lat=lines[i]['latitude']
        lon=lines[i]['longitude']

        #convert longitude and latitude to x,y
        point=ogr.Geometry(ogr.wkbPoint)#wkb represents well known binary 
        point.AddPoint(float(lon),float(lat))#order should be longitude, latitude
        point.Transform(transform)
        
        lines[i]['x']=point.GetPoint(0)[0]
        lines[i]['y']=point.GetPoint(0)[1]
        
    #output as json file
    with open('/home/jaemin/Research/MATSIM/demand_generation/infer_loc_by_pcode/matsim_sg_loc_ret_input_output/pcode_xy.json','w') as out:
        json.dump(lines,out)

if __name__ == "__main__":
    coordinate_converter()
