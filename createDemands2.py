import json
import xml.etree.cElementTree as ET

def bridge():
    #open postal code,x-y coordinates file
    with open('/home/jaemin/Research/MATSIM/demand_generation/infer_loc_by_pcode/matsim_sg_loc_ret_input_output/pcode_xy.json') as f:
        pcode_xy_lines=json.load(f)

    #initializing dictionaries
    pcode_to_xy={}
    for line in pcode_xy_lines:
        pcode_to_xy[line['pcode']]=[line['x'],line['y']]
           
    #open commuters file    
    with open('/home/jaemin/Research/MATSIM/demand_generation/infer_loc_by_pcode/matsim_sg_loc_ret_input_output/commuters.json') as f_c:
        commuters_lines=json.load(f_c)#4503 agents
    
    #set scaling factor (163 for full scale)    
    for scaling_factor in range(10,21):
        total_agents=int(scaling_factor*len(commuters_lines))
        jump=max(int(1/scaling_factor),1)
        
        #build a plan(.xml)
        plans=ET.Element("plans")
        pid=0
        tally=0
        isFull=False
        while not isFull:
            for commute in commuters_lines:
                if pid>total_agents :
                    isFull=True
                    break
                
                tally=tally+1
                if tally%jump!=0:
                    continue
    
                #read a new person's pcode
                pid=pid+1
                h_pcode=str(commute['home_pcode']).zfill(6)
                w_pcode=str(commute['fix_work_location_pcode_temp']).zfill(6)
                
                #find xy coordiate
                h_coord=pcode_to_xy[h_pcode]
                w_coord=pcode_to_xy[w_pcode]
    
                #generate xml tags (person id starts with 1)
                person=ET.SubElement(plans,"person",id=str(pid))
                plan=ET.SubElement(person,"plan")
                ET.SubElement(plan,"act",type="h",x=str(h_coord[0]),y=str(h_coord[1]), end_time="08:00")#fix departure time 
                ET.SubElement(plan,"leg",mode="car")
                ET.SubElement(plan,"act",type="w",x=str(w_coord[0]),y=str(w_coord[1]),start_time="09:00")#fix departure time
            
    
        xml=ET.ElementTree(plans)
        with open('/home/jaemin/Research/MATSIM/demand_generation/infer_loc_by_pcode/matsim_sg_loc_ret_input_output/multiplied_demands_%s_nocomeback.xml' %scaling_factor, 'w') as f:
            f.write('<?xml version="1.0" encoding="UTF-8" ?><!DOCTYPE plans SYSTEM "http://www.matsim.org/files/dtd/plans_v4.dtd">')
            xml.write(f, 'utf-8')

if __name__ == "__main__":
    bridge()
