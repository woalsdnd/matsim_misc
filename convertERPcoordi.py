import json
import ogr, osr

def coordinate_converter():
    coordi_file="/home/jaemin/Research/bitbucket_repositories/miscellaneous/expresswaysERP.json"
    with open(coordi_file) as f:
        lines=json.load(f)

    #set transformation rule
    source=osr.SpatialReference()
    source.ImportFromEPSG(4326)#WGS84
    target=osr.SpatialReference()
    target.ImportFromEPSG(3414)#SVY21 (Singapore coordinate)
    
    transform=osr.CoordinateTransformation(source,target)
    
    for i in range(len(lines)):
        lat=lines[i]['latitude']
        lon=lines[i]['longitude']

        #convert lattitude and longitude to x,y
        point=ogr.Geometry(ogr.wkbPoint)#wkb represents well known binary 
        point.AddPoint(float(lon),float(lat))#order should be longitude, latitude

        #exclude no location found
        if not (103<float(lon) and float(lon)<104 and 1<float(lat) and float(lat)<2):
            lines[i]['x']=0
            lines[i]['y']=0
            continue
        
        point.Transform(transform)
        
        lines[i]['x']=point.GetPoint(0)[0]
        lines[i]['y']=point.GetPoint(0)[1]
        
    #output as json file
    with open('/home/jaemin/Research/bitbucket_repositories/miscellaneous/expresswaysERP_xy.json','w') as out:
        json.dump(lines,out)

if __name__ == "__main__":
    coordinate_converter()
