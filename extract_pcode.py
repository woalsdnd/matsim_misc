import json
from sets import Set

def bridge():
    #open the commuters file
    commuters_file="/home/jaemin/Research/MATSIM/demand_generation/infer_loc_by_pcode/matsim_sg_loc_ret_input_output/commuters.json"
    with open(commuters_file) as f_c:
        commuters_lines=json.load(f_c)
        
    #store pcodes to a set with length of 6
    pcodes=Set([])
    for line in commuters_lines:
        pcodes.add(str(int(line['fix_work_location_pcode_temp'])).zfill(6))
        pcodes.add(str(int(line['home_pcode'])).zfill(6))
                
    #output as text file
    with open('/home/jaemin/Research/MATSIM/demand_generation/infer_loc_by_pcode/matsim_sg_loc_ret_input_output/pcode','w') as fout:
        fout.writelines("%s\n" % pcode for pcode in pcodes)
        
if __name__ == "__main__":
    bridge()
